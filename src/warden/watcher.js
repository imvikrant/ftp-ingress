const chokidar = require('chokidar');
const fs = require('fs')
const Path = require('path')
const util = require('util');
const pathConfig = require('../../ftp-config.json')
const { addSensorData } = require('../utils/api');
const readFileAsync = util.promisify(fs.readFile)

// TODO: separate out the related configs into a separate module
for (let ftpPath in pathConfig) {
  const parserPath = Path.resolve(__dirname, pathConfig[ftpPath].parserPath)
  const absPath    = Path.resolve(pathConfig[ftpPath].watchPath)
  const parserFn   = require(`${parserPath}/parser.js`)
  pathConfig[ftpPath].parserFn = parserFn
  pathConfig[ftpPath].absPath = absPath
}


const processSensorData = async (path) => {
  const fileInfo = Path.parse(path)
  const fileContent = await readFileAsync(path, 'utf-8')
  const config = pathConfig.find(({ absPath }) => absPath.startsWith(fileInfo.dir))

  const { parserFn, metaData } = config

  if (!parserFn) { throw new Error('Missing parser method') }

  // Catch and log errors while parsing to contain and avoid crashing
  try {
    const jsonData = parserFn(fileContent, fileInfo, metaData)

    if (!jsonData) console.log('Empty data', metaData.plantId)

    addSensorData(metaData.plantId, jsonData)
  } catch (e) {
    console.error(metaData.plantId, e.message)
  }
}


const startWatcher = () => {
  const paths = pathConfig.map(({ absPath }) => absPath)

  chokidar.watch([...paths], {
    ignoreInitial: true,
    awaitWriteFinish: true
  }).on('add', (path, stats) => {
    console.log(`[${path}] new file added.`)
    processSensorData(path)
  })

  console.log('Warden in watching...')
}

module.exports = {
  startWatcher
}