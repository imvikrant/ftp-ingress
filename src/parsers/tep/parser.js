const equipmentMapping = require('./equipment-mapping.json')
const parameterMapping = require('./parameter-mapping.json')

const parseJson = (strJson, fileInfo , { plantId }) => {
  let jsonData;
  try {
    jsonData = JSON.parse(strJson)
  } catch (e) {
    console.log('Invalid JSON:', e.message)
    return
  }
  const parsedJsonData = {}
  const sensorData = Object.entries(jsonData).flatMap(([sensorTime, dataPointArray]) => {
    return dataPointArray.map(dataPoint => ({sensor_time: sensorTime, ...dataPoint }))
  })

  sensorData.forEach(dataPoint => {
    const deviceType = dataPoint.dev_type;
    const equipment = equipmentMapping[plantId][deviceType]

    if (!equipment) return;
    if (!parsedJsonData[equipment]) parsedJsonData[equipment] = []

    const [device] = equipment.split('#')

    const parsedDataPoint = { sensor_time : dataPoint.sensor_time }
    Object.entries(dataPoint).forEach(([parameter, value]) => {
      const parameterName = parameterMapping[device][parameter]
      if (!parameterName) return;

      parsedDataPoint[parameterName] = value
    })

    if (device == 'mppt') {
      const currentArray = []
      Object.entries(parsedDataPoint).forEach(([parameter, value]) => {
        if (parameter.includes('current')) {
          const index = parameter.replace('current', '')
          delete parsedDataPoint[parameter]
          currentArray[index] = value
        }
      })
      parsedDataPoint['current'] = currentArray.join(',')
    }

    parsedJsonData[equipment].push(parsedDataPoint)
  })
  return parsedJsonData
}

module.exports = parseJson 