const { parse, getUnixTime } = require('../../utils/date')
const equipmentMapping = require('./equipment-mapping.json')
const parameterMapping = require('./parameter-mapping.json')
const { parseCsvToTable, splitTable, zipArray } = require('../../utils/helpers')

const parseInverterCsv = (csv, fileInfo, metaData) => {
  const table = parseCsvToTable(csv, { columnDelimeter: ';' })
  const splitTables = splitTable(table, (row) => row[0] == 'DEVICEINDEX')

  const jsonData = {}
  splitTables.forEach(table => {
    const [row1, row2, row3, row4] = table;
    const deviceIndex = row1[1]
    const modbusName = row2[1].replace('.csv', '')

    const equipment = equipmentMapping['inv'][deviceIndex]
    const indicesToParameterNameMap = parameterMapping[modbusName]
    if (!jsonData[equipment]) jsonData[equipment] = []

    if (!Array.isArray(row4) || !row4.length) return;

    const [, ...registerIndices] = row3;
    const [sensorTime, ...registerValues] = row4;

    const sensorDataPoint = { sensor_time: getUnixTime(parse(`${sensorTime} +0530`, 'yy/MM/dd-HH:mm:ss X', new Date())) }
    const csvParameters = ['current', 'voltage', 'power']
    const csvParameterData = {}
    zipArray(registerIndices, registerValues).forEach(([registerIndex, value]) => {
      const parameterName = indicesToParameterNameMap[registerIndex]

      if (!parameterName) return;

      if (csvParameters.includes(parameterName)) {
        if (!csvParameterData[parameterName]) { csvParameterData[parameterName] = [] }

        return csvParameterData[parameterName].push(value)
      }

      sensorDataPoint[parameterName] = value
    })

    Object.entries(csvParameterData).forEach(([parameter, valueArray]) => {
      sensorDataPoint[parameter] = valueArray.join(',')
    })

    jsonData[equipment].push(sensorDataPoint)
  })

  return jsonData
}

const parseWeatherStationCsv = (csv) => {

  const table = parseCsvToTable(csv, { columnDelimeter: ';' })
  const splitTables = splitTable(table, (row) => row[0] == 'TypeIO')

  const jsonData = {}
  splitTables.forEach(table => {
    const [row1, row2, row3] = table;
    const modbusName = row1[1].replace('.csv', '')

    const equipment = equipmentMapping['ws'][1]
    const indicesToParameterNameMap = parameterMapping[modbusName]
    jsonData[equipment] = []

    if (!Array.isArray(row3) || !row3.length) return;

    const [, ...registerIndices] = row2;
    const [sensorTime, ...registerValues] = row3;

    const sensorDataPoint = { sensor_time: getUnixTime(parse(`${sensorTime} +0530`, 'yy/MM/dd-HH:mm:ss X', new Date())) }
    zipArray(registerIndices, registerValues).forEach(([registerIndex, value]) => {
      const parameterName = indicesToParameterNameMap[registerIndex]

      if (!parameterName) return;

      sensorDataPoint[parameterName] = value
    })

    jsonData[equipment].push(sensorDataPoint)
  })

  return jsonData
}

const parseCsv = (data, fileInfo) => {
  const filename = fileInfo.name
  if (/^WPM00D22F_MODBUS_/.test(filename)) {
    return parseInverterCsv(data)
  }
  
  if (/^WPM00D22F_IO_/.test(filename)) {
    return parseWeatherStationCsv(data)
  }
}

module.exports = parseCsv