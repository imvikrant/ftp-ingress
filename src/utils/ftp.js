const { Readable } = require('stream')
const csv = require('csv')
const _ = require('lodash')
const { getUnixTime } = require('date-fns')

const ftpWriteCsv = async (ftp, data, filePath) => {
  const readStream = Readable.from(data, { objectMode: true })

  await ftp.put(readStream.pipe(csv.stringify({
    header: true,
    quoted: false
  })), filePath)
}

const ftpReadFile = async (ftp, filePath) => {
  const stream = await ftp.get(filePath)

  const chunks = []
  for await (let chunk of stream) {
    chunks.push(chunk)
  }

  return Buffer.concat(chunks).toString('utf-8')
}

const ftpReadLatestFile = async (ftp, dirPath) => {
  const files = await ftp.list(dirPath).filter(file => file.size)
  const latestFile = _.maxBy(files, (file) => getUnixTime(new Date(file.date)))

  if (!latestFile) return;
  
  const latestFilePath = `${dirPath}/${latestFile.name}`

  return ftpReadFile(ftp, latestFilePath)
}


module.exports = {
  ftpReadFile,
  ftpReadLatestFile,
  ftpWriteCsv
}