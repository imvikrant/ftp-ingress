require('dotenv').config()

module.exports = {
  m2mEndpoint: process.env.M2M_ENDPOINT,
}
