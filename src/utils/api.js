const axios = require('axios')
const { isWholeNumber, isPlainObject } = require('./assert')
const { expandDeviceId } = require('./device')
const config = require('./config')

const API_PREFIX = `${config.m2mEndpoint}/api/v1.0`

const endpointMap = {
  'inv' : `${API_PREFIX}/pushInverter`,
  'mfm' : `${API_PREFIX}/pushInputFeeder`,
  'ws'  : `${API_PREFIX}/pushWs`,
  'mppt': `${API_PREFIX}/pushMppt`,
}

const pushData = (device, plantId, data) => {
  const endPoint = endpointMap[device]
  return axios.post(endPoint, { plant_id: plantId, data })
}

// Group the parsed data by device and expand the device id into given block_id, room_id...
// for the given device and push the data to the M2M endpoint
const addSensorData = (plantId, data) => {
  if (!isWholeNumber(parseFloat(plantId))) throw new Error('plant id must be an Integer')
  if (!isPlainObject(data)) throw new Error('data must be a plain object')

  const groupedByDevice = {}

  Object.entries(data).forEach(([equipment, dataPoints]) => {
    const [device, deviceId] = equipment.split('#')
    const expandedId = expandDeviceId(device, deviceId)

    if (!groupedByDevice[device]) groupedByDevice[device] = []

    const expandedDataPoints = dataPoints.map(dataPoint => ({...dataPoint, ...expandedId}))
    groupedByDevice[device].push(...expandedDataPoints)
  })
  
  Object.entries(groupedByDevice).forEach(async ([device, data]) => {
    const response = await pushData(device, plantId, data).catch(e => {
      console.log(`[${device} ${plantId} ${Math.round(Date.now() / 1000)}] M2M Push Error -> `, e.message)
    })
    console.log(`[${device} ${plantId} ${Math.round(Date.now() / 1000)}] ${response.status} ${ response.data }`)
  })
}

module.exports = {
  addSensorData,
}
