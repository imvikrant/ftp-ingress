const deviceInformation = {
  'inv': {
    idList: ['block_id', 'inverter_room', 'inverter_id']
  },
  'mfm': {
    idList: ['block_id', 'feeder_id']
  },
  'ws': {
    idList: ['block_id', 'ws_id']
  },
  'mppt': {
    idList: ['block_id', 'inverter_room', 'inverter_id']
  },
}

const getDeviceIdList = (device) => {
  if (!deviceInformation[device]) {
    throw new Error('Device Not Found')
  }

  return [...deviceInformation[device].idList]
}

const formDeviceId = (device, data) => { 
  const idList = getDeviceIdList(device);

  return idList.map(idName => data[idName]).join('-')
}

const expandDeviceId = (device, deviceId) => {
  const idArr = deviceId.split('-')
  const idNameList = getDeviceIdList(device)

  if (idArr.length !== idNameList.length) return null;

  const expandedDeviceIds = idArr.map((id, index) => [idNameList[index], id])

  return Object.fromEntries(expandedDeviceIds)
}

module.exports =  {
  getDeviceIdList,
  formDeviceId,
  expandDeviceId
}