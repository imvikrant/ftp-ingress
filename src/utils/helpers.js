const _ = require('lodash');

const parseCsvToTable = (csv, { rowDelimeter = '\n', columnDelimeter = ',' }) => {
  return csv.split(rowDelimeter).map(line => line.split(columnDelimeter))
}

const splitTable = (table, splitFn) => {
  const splitTables = []
  const splitIndices = []

  const rowCount = table.length;
  table.forEach((row, i) => {
    if (splitFn(row) || i == rowCount - 1) splitIndices.push(i)
  })

  for (let i = 0; i < splitIndices.length - 1; i++) {
    splitTables.push(table.slice(splitIndices[i], splitIndices[i + 1]))
  }

  return splitTables
}

const zipArray = (...args) => _.zip(...args);

module.exports = {
  parseCsvToTable,
  splitTable,
  zipArray
}