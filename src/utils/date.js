const dateFns = require('date-fns')

const parse = (...args) => dateFns.parse(...args)

const getUnixTime = (...args) => dateFns.getUnixTime(...args)

module.exports = {
  parse,
  getUnixTime
}