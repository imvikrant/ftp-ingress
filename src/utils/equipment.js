const prefixMap = {
  'inv': 'inverter',
  'ws' : 'weather station',
  "mppt": 'mppt',
  "mfm": "mfm"
}

module.exports = {
  prefixMap
}