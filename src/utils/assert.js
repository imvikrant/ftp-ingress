const _ = require('lodash')

const isInteger = (value) => _.isInteger(value);

const isWholeNumber = (value) => isInteger(value) && value >= 0;

const isPlainObject = (object) => _.isPlainObject(object)

module.exports = {
  isWholeNumber,
  isInteger,
  isPlainObject
}